package com.aberke.mailservice.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Representation of a sent e-mail to a single recipient.
 */
@Entity
@Table(name = "Sent", indexes = {
        @Index(name = "idx_recipient_notified", columnList = "recipient,notifiedUndeliverable"),
        @Index(name = "idx_sent", columnList = "sentTimestamp")})
@Getter
@Setter
@ToString
public class SentMail {

    public enum RecipientType {
        TO, CC, BCC
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private long sentTimestamp;
    @Column
    private String sender;
    @Column
    private String recipient;
    @Column
    private RecipientType recipientType;
    @Column(nullable = false)
    private String subject;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String body;
    @Column
    private boolean notifiedUndeliverable;

}
