package com.aberke.mailservice.data;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.aberke.mailservice.data.MailAddressConverter.cleanCommaSeparatedStream;
import static com.aberke.mailservice.data.SentMail.RecipientType;
import static com.aberke.mailservice.data.SentMail.RecipientType.*;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Converting {@link OutboxMail} to list of {@link SentMail} entities with the addresses separated.
 */
@Component
@AllArgsConstructor
public class SentMailConverter {

    private final MailAddressConverter addressConverter;

    public List<SentMail> convert(OutboxMail outboxMail, Instant sentAt) {
        List<SentMail> mails = new ArrayList<>();
        mails.addAll(convert(outboxMail, outboxMail.getTo(), TO, sentAt));
        mails.addAll(convert(outboxMail, outboxMail.getCc(), CC, sentAt));
        mails.addAll(convert(outboxMail, outboxMail.getBcc(), BCC, sentAt));
        return mails;
    }

    private List<SentMail> convert(OutboxMail outboxMail, String addressList, RecipientType type, Instant sentAt) {
        if (addressList == null) {
            return emptyList();
        }
        return cleanCommaSeparatedStream(addressList)
                .map(s -> toSentMail(addressConverter.convertAddress(s), type, sentAt, outboxMail))
                .collect(toList());
    }

    private SentMail toSentMail(MailAddress address, RecipientType type, Instant sentAt, OutboxMail outboxMail) {
        SentMail mail = new SentMail();
        mail.setSubject(outboxMail.getSubject());
        mail.setBody(outboxMail.getBody());
        mail.setSender(outboxMail.getSender());
        mail.setRecipient(address.getEmailAddress());
        mail.setRecipientType(type);
        mail.setSentTimestamp(sentAt.toEpochMilli());
        return mail;
    }
}
