package com.aberke.mailservice.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Storing Sent emails (scheduled through external sender API).
 */
@Repository
public interface SentMailRepository extends CrudRepository<SentMail, Integer> {

    List<SentMail> findByRecipientAndNotifiedUndeliverableOrderBySentTimestampAsc(
            String email, boolean notifiedUndeliverable);

    /**
     * Returning emails for the recipient that were sent and do not have an 'undeliverable' flag yet.
     *
     * @param email Recipient email address
     * @return Emails sent for recipient that are available in the log with 'deliverable' status, ordered by sent time ascending.
     */
    default List<SentMail> findSentMailsOfRecipient(String email) {
        return findByRecipientAndNotifiedUndeliverableOrderBySentTimestampAsc(email, false);
    }
}
