package com.aberke.mailservice.data;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Abstraction over e-mails to be sent via {@link com.aberke.mailservice.domain.sender.MailSenderApi}
 */
public interface Mail {

    @NonNull
    String getSender();

    void setSender(@NonNull String sender);

    @Nullable
    String getTo();

    void setTo(@Nullable String to);

    @Nullable
    String getCc();

    void setCc(@Nullable String cc);

    @Nullable
    String getBcc();

    void setBcc(@Nullable String bcc);

    @NonNull
    String getSubject();

    void setSubject(@NonNull String subject);

    @NonNull
    String getBody();

    void setBody(@NonNull String body);


}
