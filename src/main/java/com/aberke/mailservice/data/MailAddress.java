package com.aberke.mailservice.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Represents an e-mail address with an optional name.
 */
@Getter
@AllArgsConstructor
public class MailAddress {

    @NonNull
    private final String emailAddress;
    @Nullable
    private final String name;

}
