package com.aberke.mailservice.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Representation of an e-mail scheduled for sending.
 */
@Entity
@Table(name = "Outbox", indexes = {
        @Index(name = "idx_scheduled", columnList = "scheduledTimestamp"),
        @Index(name = "idx_undeliverable", columnList = "undeliverable")
})
@Getter
@Setter
@ToString
public class OutboxMail implements Mail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private long scheduledTimestamp;
    @Column(nullable = false)
    private String sender;
    @Column
    private String to;
    @Column
    private String cc;
    @Column
    private String bcc;
    @Column(nullable = false)
    private String subject;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String body;
    @Column
    private int retryCount;
    @Column
    private boolean undeliverable;

    public void incrementRetryCount() {
        retryCount++;
    }

    public void delayScheduledTime(int retryBackoffMillis) {
        this.scheduledTimestamp += retryBackoffMillis;
    }

}
