package com.aberke.mailservice.data;

import com.aberke.mailservice.api.validation.NameAndEmailAddressPattern;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.commaDelimitedListToStringArray;

/**
 * Converts e-mail addresses and comma separated e-mail lists to {@link MailAddress} objects.
 * 
 * Supports both standalone and angle bracket e-mails.
 * e.g. "John Smiths <john.smith@example.com>" or "abc123@example.com"
 */
@Component
public class MailAddressConverter {

    public MailAddress convertAddress(String address) {

        String email;
        String name;

        Matcher nameAndEmailMatcher = NameAndEmailAddressPattern.PARSER_PATTERN.matcher(address);
        if (nameAndEmailMatcher.matches()) {
            // Address contains both name and e-mail address
            name = nameAndEmailMatcher.group(1).trim();
            email = nameAndEmailMatcher.group(2);
        } else {
            // Standalone e-mail address, no name
            name = null;
            email = address;
        }

        return new MailAddress(email, StringUtils.isEmpty(name) ? null : name);
    }

    public List<MailAddress> convertAddressList(String list) {
        return cleanCommaSeparatedStream(list)
                .map(this::convertAddress)
                .collect(toList());
    }

    /**
     * Takes a list of comma separated values and returns stream of clean inputs.
     *
     * @param list Non-null list of comma separated values.
     * @return Stream of trimmed, non-empty items on the list.
     */
    public static Stream<String> cleanCommaSeparatedStream(String list) {
        return stream(commaDelimitedListToStringArray(list))
                .map(String::trim)
                .filter(s -> !s.isEmpty());
    }
}
