package com.aberke.mailservice.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Storing Outbox emails (scheduled for sending).
 */
@Repository
public interface OutboxMailRepository extends PagingAndSortingRepository<OutboxMail, Integer> {

    /**
     * Returning the next batch of e-mails in the outbox scheduled for delivery.
     *
     * @param batchSize Maximum number of entities returned.
     * @param untilExclusive Maximum timestamp of the scheduled entries, exclusive.
     * @return Pending outbox e-mails ordered by creation date ascending, before the given timestamp.
     */
    default List<OutboxMail> getNextBatch(int batchSize, Instant untilExclusive) {
        PageRequest pageParams = PageRequest.of(0, batchSize, Sort.by("scheduledTimestamp"));
        Page<OutboxMail> firstPage = findByUndeliverableAndScheduledTimestampLessThan(false, untilExclusive.toEpochMilli(), pageParams);
        return firstPage.getContent();
    }

    // SELECT fields FROM Outbox WHERE
    //   undeliverable = ? AND
    //   scheduledTimestamp < ?
    // ORDER BY scheduledTimestamp ASC
    // LIMIT (limit)
    Page<OutboxMail> findByUndeliverableAndScheduledTimestampLessThan(
            boolean undeliverable, long scheduledTimeLessThan, Pageable pageable);

}
