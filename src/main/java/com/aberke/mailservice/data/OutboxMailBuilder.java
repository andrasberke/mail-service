package com.aberke.mailservice.data;

import org.springframework.util.StringUtils;

/**
 * Builder for instantiating an {@link OutboxMail} entity.
 */
public class OutboxMailBuilder {

    private final OutboxMail mail;

    public OutboxMailBuilder() {
        this.mail = new OutboxMail();
    }

    public OutboxMailBuilder setSubject(String subject) {
        mail.setSubject(subject);
        return this;
    }

    public OutboxMailBuilder setBody(String body) {
        mail.setBody(body);
        return this;
    }

    public OutboxMailBuilder setSender(String from) {
        mail.setSender(from);
        return this;
    }

    public OutboxMailBuilder setTo(String to) {
        mail.setTo(to);
        return this;
    }

    public OutboxMailBuilder setCc(String cc) {
        mail.setCc(cc);
        return this;
    }

    public OutboxMailBuilder setBcc(String bcc) {
        mail.setBcc(bcc);
        return this;
    }

    public OutboxMail build() {
        if (StringUtils.isEmpty(mail.getSender())) {
            throw new IllegalStateException("Must set 'sender' first");
        }
        if (StringUtils.isEmpty(mail.getSubject())) {
            throw new IllegalStateException("Must set 'subject' first");
        }
        if (StringUtils.isEmpty(mail.getBody())) {
            throw new IllegalStateException("Must set 'body' first");
        }
        return mail;
    }

}
