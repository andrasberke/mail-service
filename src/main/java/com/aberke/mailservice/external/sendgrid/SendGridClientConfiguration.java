package com.aberke.mailservice.external.sendgrid;

import com.sendgrid.SendGrid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Injects SendGrid API object and API key.
 */
@Configuration
@ConditionalOnProperty(name = "com.aberke.mailservice.provider", havingValue = "sendgrid")
public class SendGridClientConfiguration {

    @Value("${com.aberke.mailservice.provider.sendgrid.apikey}")
    private String apiKey;

    @Bean
    public SendGrid createSendGridClient() {
        return new SendGrid(apiKey);
    }
}
