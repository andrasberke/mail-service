package com.aberke.mailservice.external.sendgrid.converters;

import com.aberke.mailservice.external.sendgrid.data.GetBounceResponse;
import com.aberke.mailservice.external.sendgrid.data.GetBounceResponseEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Converts Bounce API response of SendGrid into {@link GetBounceResponse} and validates received fields.
 */
@Component
@AllArgsConstructor
public class BouncesResponseConverter {

    private final Validator validator;
    private final ObjectMapper objectMapper;

    public GetBounceResponse convert(String responseBody) throws IOException {
        try {
            List<GetBounceResponseEntry> entries = objectMapper.readValue(responseBody, new TypeReference<>() {});
            entries.forEach(this::validateEntry);
            return new GetBounceResponse(entries);
        } catch (JsonProcessingException jpe) {
            throw new IOException("Cannot parse response", jpe);
        } catch (ValidationException ve) {
            throw new IOException("Get bounces response is invalid ", ve);
        }
    }

    private void validateEntry(GetBounceResponseEntry responseEntry) {
        Set<ConstraintViolation<GetBounceResponseEntry>> errors = validator.validate(responseEntry);
        if (!errors.isEmpty()) {
            throw new ValidationException(errors.toString());
        }
    }

}
