package com.aberke.mailservice.external.sendgrid.endpoints;

import com.aberke.mailservice.data.Mail;
import com.aberke.mailservice.domain.sender.MailSenderApiException;
import com.aberke.mailservice.external.sendgrid.converters.SendGridMailConverter;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static org.springframework.http.HttpStatus.ACCEPTED;

/**
 * Executing SendGrid request of mail/send.
 */
@Component
@ConditionalOnProperty(name = "com.aberke.mailservice.provider", havingValue = "sendgrid")
@AllArgsConstructor
@Slf4j
public class MailSend {

    private final SendGrid sendGrid;
    private final SendGridMailConverter mailConverter;

    public void post(Mail mail) {
        log.debug("Sending mail ...");
        try {
            Request request = buildMailRequest(mail);
            Response response = sendGrid.api(request);
            handleMailResponse(response);
        } catch (IOException e) {
            throw new MailSenderApiException("Error while calling SendGrid", e);
        }
        log.debug("Done");
    }

    private Request buildMailRequest(Mail mail) throws IOException {
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mailConverter.mailToSendGridMail(mail).build());
        return request;
    }

    private void handleMailResponse(Response response) throws IOException {
        log.debug("Received response:");
        int responseStatus = response.getStatusCode();
        String responseBody = response.getBody();
        log.debug("\tStatus: {}", responseStatus);
        log.debug("\tBody: {}", responseBody);
        throwIfNotOk(responseStatus, responseBody);
    }

    private void throwIfNotOk(int responseStatus, String responseBody) throws IOException {
        if (responseStatus != ACCEPTED.value() || responseBody == null) {
            throw new IOException("Unexpected response " + responseStatus + ": " + responseBody);
        }
    }

}
