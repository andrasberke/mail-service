package com.aberke.mailservice.external.sendgrid.data;

import com.aberke.mailservice.domain.bounces.BounceInformation;
import com.aberke.mailservice.domain.bounces.BounceList;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Request structure of SendGrid bounce delete endpoint.
 */
public class DeleteBounceRequest {

    private List<String> emails;

    public DeleteBounceRequest(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getEmails() {
        return emails;
    }

    public static DeleteBounceRequest buildDeleteRequest(BounceList bounces) {
        List<String> emails = bounces
                .getEntries()
                .stream()
                .map(BounceInformation::getEmail)
                .collect(toList());
        return new DeleteBounceRequest(emails);
    }

}
