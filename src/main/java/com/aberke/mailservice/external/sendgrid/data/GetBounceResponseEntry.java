package com.aberke.mailservice.external.sendgrid.data;

import com.aberke.mailservice.domain.bounces.BounceInformation;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

/**
 * Response structure of SendGrid bounce record.
 * 
 * Parameters annotations are validated by converter.
 */
@Setter
@Getter
@ToString
public class GetBounceResponseEntry implements BounceInformation {

    @Positive
    private long created;
    @NotEmpty
    private String email;
    @NotEmpty
    private String reason;
    @NotEmpty
    private String status;

    public GetBounceResponseEntry() {
        email = "";
        reason = "";
        status = "";
    }

}
