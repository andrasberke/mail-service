package com.aberke.mailservice.external.sendgrid.endpoints;

import com.aberke.mailservice.domain.bounces.BounceList;
import com.aberke.mailservice.domain.sender.MailSenderApiException;
import com.aberke.mailservice.external.sendgrid.converters.BouncesResponseConverter;
import com.aberke.mailservice.external.sendgrid.data.GetBounceResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

import static com.aberke.mailservice.external.sendgrid.data.DeleteBounceRequest.buildDeleteRequest;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

/**
 * Executing GET and DELETE request of SendGrid suppression/bounces.
 */
@Component
@ConditionalOnProperty(name = "com.aberke.mailservice.provider", havingValue = "sendgrid")
@AllArgsConstructor
@Slf4j
public class Bounces {

    private final SendGrid sendGrid;
    private final BouncesResponseConverter responseConverter;
    private final ObjectMapper objectMapper;

    public GetBounceResponse get() {
        log.debug("Checking bounces ...");
        try {
            Request request = new Request();
            request.setMethod(Method.GET);
            request.setEndpoint("suppression/bounces");
            Response response = sendGrid.api(request);
            String body = response.getBody();
            throwIfNotOk(OK, response.getStatusCode(), body);
            if (!StringUtils.isEmpty(body)) {
                log.debug("Received {}", body);
            }
            return responseConverter.convert(body);
        } catch (IOException e) {
            throw new MailSenderApiException("Error while calling SendGrid", e);
        }
    }

    public void delete(BounceList bounces) {
        log.debug("Deleting bounces ...");
        try {
            Request request = new Request();
            request.setMethod(Method.DELETE);
            request.setEndpoint("suppression/bounces");
            request.setBody(objectMapper.writeValueAsString((buildDeleteRequest(bounces))));
            Response response = sendGrid.api(request);
            throwIfNotOk(NO_CONTENT, response.getStatusCode(), response.getBody());
        } catch (IOException e) {
            throw new MailSenderApiException("Error while calling SendGrid", e);
        }
        log.debug("Done");
    }

    private void throwIfNotOk(HttpStatus expectedStatus, int responseStatus, String responseBody) throws IOException {
        boolean missingBody = responseStatus != NO_CONTENT.value() && responseBody == null;
        if (expectedStatus.value() != responseStatus || missingBody) {
            throw new IOException("Unexpected response " + responseStatus + ": " + responseBody);
        }
    }

}
