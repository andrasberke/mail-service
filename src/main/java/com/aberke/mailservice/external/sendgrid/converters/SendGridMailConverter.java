package com.aberke.mailservice.external.sendgrid.converters;

import com.aberke.mailservice.data.Mail;
import com.aberke.mailservice.data.MailAddress;
import com.aberke.mailservice.data.MailAddressConverter;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Personalization;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Converts a {@link Mail} instance to a SendGrid mail object ready to be sent.
 */
@Component
@AllArgsConstructor
public class SendGridMailConverter {

    private final MailAddressConverter addressConverter;

    public com.sendgrid.Mail mailToSendGridMail(Mail mail) {

        // Subject, body
        String subject = mail.getSubject();
        Content content = new Content("text/plain", mail.getBody());

        // From
        MailAddress fromAddress = addressConverter.convertAddress(mail.getSender());
        Email from = addressToSendGrid(fromAddress);

        // To
        Personalization personalization = new Personalization();
        String to = mail.getTo();
        if (to != null) {
            for (MailAddress address : addressConverter.convertAddressList(to)) {
                personalization.addTo(addressToSendGrid(address));
            }
        }
        // CC
        String cc = mail.getCc();
        if (cc != null) {
            for (MailAddress address : addressConverter.convertAddressList(cc)) {
                personalization.addCc(addressToSendGrid(address));
            }
        }
        // BCC
        String bcc = mail.getBcc();
        if (bcc != null) {
            for (MailAddress address : addressConverter.convertAddressList(bcc)) {
                personalization.addBcc(addressToSendGrid(address));
            }
        }

        // Build result
        com.sendgrid.Mail result = new com.sendgrid.Mail();
        result.setFrom(from);
        result.setSubject(subject);
        result.addContent(content);
        result.addPersonalization(personalization);
        return result;
    }

    private Email addressToSendGrid(MailAddress addressEntry) {
        // Note that name is optional (nullable) on the SendGrid API as well
        return new Email(addressEntry.getEmailAddress(), addressEntry.getName());
    }
}
