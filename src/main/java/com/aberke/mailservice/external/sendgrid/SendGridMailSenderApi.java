package com.aberke.mailservice.external.sendgrid;

import com.aberke.mailservice.data.Mail;
import com.aberke.mailservice.domain.bounces.BounceList;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import com.aberke.mailservice.domain.sender.MailSenderApiException;
import com.aberke.mailservice.external.sendgrid.endpoints.Bounces;
import com.aberke.mailservice.external.sendgrid.endpoints.MailSend;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link MailSenderApi} that uses the SendGrid API for sending e-mails.
 */
@Service
@ConditionalOnProperty(name = "com.aberke.mailservice.provider", havingValue = "sendgrid")
@AllArgsConstructor
public class SendGridMailSenderApi implements MailSenderApi {

    private final MailSend mailSendEndpoint;
    private final Bounces bouncesEndpoint;

    /**
     * Calls SendGrid API to send an e-mail.
     *
     * @param mail Domain object, email to send.
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    @Override
    public void sendMail(Mail mail) throws MailSenderApiException {
        mailSendEndpoint.post(mail);
    }

    /**
     * Calls SendGrid API to query undelivered e-mails.
     *
     * @return List of bounces returned by the mailer API.
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    @Override
    public BounceList queryBounces() throws MailSenderApiException {
        return bouncesEndpoint.get();
    }

    /**
     * Calls SendGrid API to delete all bounce events.
     *
     * @param bounces List of entries to remove
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    @Override
    public void deleteBounces(BounceList bounces) throws MailSenderApiException {
        bouncesEndpoint.delete(bounces);
    }

}
