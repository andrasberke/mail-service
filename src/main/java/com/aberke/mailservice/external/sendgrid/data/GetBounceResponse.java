package com.aberke.mailservice.external.sendgrid.data;

import com.aberke.mailservice.domain.bounces.BounceList;

import java.util.List;

/**
 * Response structure of SendGrid bounce records.
 */
public class GetBounceResponse implements BounceList {

    private List<GetBounceResponseEntry> entries;

    public GetBounceResponse(List<GetBounceResponseEntry> entries) {
        this.entries = entries;
    }

    @Override
    public List<GetBounceResponseEntry> getEntries() {
        return entries;
    }

    @Override
    public boolean isEmpty() {
        return entries.isEmpty();
    }
}
