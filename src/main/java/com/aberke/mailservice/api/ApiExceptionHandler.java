package com.aberke.mailservice.api;

import com.aberke.mailservice.api.data.ApiError;
import com.aberke.mailservice.api.data.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

import static com.aberke.mailservice.api.data.Response.apiResponse;
import static com.aberke.mailservice.api.data.Response.apiResponseObject;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Handling API validation errors and uncaught general exceptions.
 */
@ControllerAdvice
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException argNotValidException,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        log.debug("Handling validation error", argNotValidException);

        List<ApiError> errors = argNotValidException.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(e -> ApiError.ofValidationError(e.getDefaultMessage(), e.getField(), e.getRejectedValue()))
                .collect(Collectors.toList());

        return apiResponseObject(BAD_REQUEST, "Validation error while processing the request", errors);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<ApiResponse> exception(Exception exception) {
        log.debug("Handling generic exception", exception);
        return apiResponse(INTERNAL_SERVER_ERROR, "Error while processing request", ApiError.ofException(exception));
    }
}
