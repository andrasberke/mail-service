package com.aberke.mailservice.api.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Data object for API error responses.
 *
 * Used for validation errors and generic uncaught exceptions.
 */
@Getter
@Setter
@AllArgsConstructor
public class ApiError {

    public static final String TYPE_GENERIC_ERROR = "generic";
    public static final String TYPE_VALIDATION_ERROR = "validation";

    @NonNull
    private String type;
    @NonNull
    private String message;
    @Nullable
    private String field;
    @Nullable
    private String value;

    public ApiError(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public static ApiError ofException(Exception exception) {
        return new ApiError(TYPE_GENERIC_ERROR, exception.getClass().getSimpleName() + ": " + exception.getMessage());
    }

    public static ApiError ofValidationError(String message, String field, @Nullable Object value) {
        return new ApiError(TYPE_VALIDATION_ERROR, message, field, value == null ? "null" : value.toString());
    }

}
