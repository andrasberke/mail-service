package com.aberke.mailservice.api.data;

import com.aberke.mailservice.api.validation.EmailAddress;
import com.aberke.mailservice.api.validation.EmailAddressList;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * Data object for client request body.
 * 
 * Parameters are validated with JSR 380 annotations.
 */
@Getter
@Setter
public class MailRequest {

    // Mandatory field of sender email address, supports both standalone and angle bracket addresses,
    // e.g. "John Smiths <john.smith@example.com>" or "abc123@example.com"
    @EmailAddress
    private String from;

    // Mandatory list of recipients, supports both standalone and angle bracket addresses,
    // e.g. "John Smiths <john.smith@example.com>, abc123@example.com, bca@example.com"
    @EmailAddressList
    private String to;

    // Optional list of CC recipients, supports both standalone and angle bracket addresses
    @EmailAddressList(optional = true)
    private String cc;

    // Optional list of BCC recipients, supports both standalone and angle bracket addresses
    @EmailAddressList(optional = true)
    private String bcc;

    // Mandatory subject for the e-mail
    @NotEmpty
    private String subject;

    // Mandatory plain text body for the e-mail
    @NotEmpty
    private String body;

}
