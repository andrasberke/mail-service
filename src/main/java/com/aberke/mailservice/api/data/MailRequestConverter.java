package com.aberke.mailservice.api.data;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailBuilder;
import org.springframework.stereotype.Component;

/**
 * Converts an incoming API {@link MailRequest} to a {@link OutboxMail} domain entity.
 * 
 * Assumes that the request parameters have been already validates.
 */
@Component
public class MailRequestConverter {

    public OutboxMail requestToMail(MailRequest request) {
        return new OutboxMailBuilder()
                .setSender(request.getFrom())
                .setTo(request.getTo())
                .setCc(request.getCc())
                .setBcc(request.getBcc())
                .setSubject(request.getSubject())
                .setBody(request.getBody())
                .build();
    }

}
