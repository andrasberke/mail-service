package com.aberke.mailservice.api.data;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;

/**
 * Data object for API responses.
 * 
 * Contains readable status message and an optional list of errors.
 */
@Getter
@Setter
public class ApiResponse {

    private int status;
    @NonNull
    private String message;
    @Nullable
    private List<ApiError> errors;

    public ApiResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public ApiResponse(HttpStatus status, String message) {
        this.status = status.value();
        this.message = message;
    }

    public void setError(ApiError error) {
        this.errors = new ArrayList<>(singletonList(error));
    }
}
