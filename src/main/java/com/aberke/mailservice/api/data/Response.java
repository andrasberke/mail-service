package com.aberke.mailservice.api.data;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Convenience functions for returning response entities of {@link ApiResponse}.
 */
public class Response {

    public static ResponseEntity<ApiResponse> apiResponse(int status, String message) {
        return apiResponse(status, message, null);
    }

    public static ResponseEntity<ApiResponse> apiResponse(HttpStatus status, String message) {
        return apiResponse(status.value(), message, null);
    }

    public static ResponseEntity<ApiResponse> apiResponse(HttpStatus status, String message, ApiError error) {
        return apiResponse(status.value(), message, error);
    }

    private static ResponseEntity<ApiResponse> apiResponse(int status, String message, @Nullable ApiError error) {
        ApiResponse response = new ApiResponse(status, StringUtils.isEmpty(message) ? "Not available" : message);
        if (error != null) {
            response.setError(error);
        }
        return ResponseEntity.status(status).body(response);
    }

    public static ResponseEntity<Object> apiResponseObject(HttpStatus status, String message, List<ApiError> errors) {
        ApiResponse response = new ApiResponse(status, message);
        response.setErrors(errors);
        return ResponseEntity.status(status).body(response);
    }
}
