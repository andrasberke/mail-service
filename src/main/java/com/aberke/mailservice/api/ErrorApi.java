package com.aberke.mailservice.api;

import com.aberke.mailservice.api.data.ApiResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import static com.aberke.mailservice.api.data.Response.apiResponse;

/**
 * REST controller for the /error endpoint mapping.
 */
@Controller
public class ErrorApi implements ErrorController {

    @GetMapping("/error")
    public ResponseEntity<ApiResponse> error(HttpServletRequest req) {
        int status = (Integer) req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        String message = (String) req.getAttribute(RequestDispatcher.ERROR_MESSAGE);
        return apiResponse(status, message);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
