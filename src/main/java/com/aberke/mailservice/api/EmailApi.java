package com.aberke.mailservice.api;

import com.aberke.mailservice.api.data.ApiResponse;
import com.aberke.mailservice.api.data.MailRequest;
import com.aberke.mailservice.api.data.MailRequestConverter;
import com.aberke.mailservice.domain.OutboxService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.aberke.mailservice.api.data.Response.apiResponse;
import static org.springframework.http.HttpStatus.OK;

/**
 * REST controller for the e-mail API "/send" endpoint.
 */
@RestController
@AllArgsConstructor
public class EmailApi {

    private final OutboxService outbox;
    private final MailRequestConverter requestConverter;

    @PostMapping("/send")
    public ResponseEntity<ApiResponse> sendEmail(@Valid @RequestBody MailRequest postBody) {
        outbox.add(requestConverter.requestToMail(postBody));
        return apiResponse(OK, "E-mail scheduled for sending");
    }
}
