package com.aberke.mailservice.api.validation;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Input validator for e-mail address
 * Supports both standalone and angle bracket e-mails.
 * 
 * Example of valid inputs: "John Smiths <john.smith@example.com>", "abc123@example.com"
 * Example of invalid inputs: "abc", "abc@@"
 */
public class EmailAddressValidator implements ConstraintValidator<EmailAddress, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        return EmailAddressPattern.MATCH_PREDICATE
                .or(NameAndEmailAddressPattern.MATCH_PREDICATE)
                .test(value);
    }
}
