package com.aberke.mailservice.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.function.Predicate;

import static com.aberke.mailservice.data.MailAddressConverter.cleanCommaSeparatedStream;
import static java.util.stream.Collectors.toList;

/**
 * Input validator for list of e-mail address, separated by commas.
 * 
 * Supports both standalone and angle bracket e-mails.
 * If the 'optional' field is set, null input is also accepted.
 * 
 * Example of valid list: "John Smiths <john.smith@example.com>, abc123@example.com, bca@example.com"
 * Example of invalid list: "abc,abc123@example.com"
 */
public class EmailAddressListValidator implements ConstraintValidator<EmailAddressList, String> {

    private boolean optional;

    @Override
    public void initialize(EmailAddressList emailAddressList) {
        this.optional = emailAddressList.optional();
    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {

        if (field == null) {
            // Missing field only valid if it is declared optional
            return optional;
        }

        List<String> addresses = cleanCommaSeparatedStream(field).collect(toList());

        if (addresses.isEmpty()) {
            // No valid items in the list
            return false;
        }

        // Each item must be either standalone e-mail address or name with e-mail
        Predicate<String> matchPredicate = EmailAddressPattern.MATCH_PREDICATE
                .or(NameAndEmailAddressPattern.MATCH_PREDICATE);
        return addresses.stream().allMatch(matchPredicate);
    }
}
