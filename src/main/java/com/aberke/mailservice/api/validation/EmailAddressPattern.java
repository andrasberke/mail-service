package com.aberke.mailservice.api.validation;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class EmailAddressPattern {

    /**
     * Regexp for matching e-mail address.
     * Note that it is not comprehensive.
     * Source: http://regexlib.com/
      */
    public static final String EMAIL_REGEXP = "([a-zA-Z0-9_\\-\\.]+)@(([a-zA-Z0-9\\-]+\\.)+)([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
    public static final String EMAIL_REGEXP_FULL_LINE = "^" + EMAIL_REGEXP + "$";

    /**
     * Predicate for matching strings using the email validation regexp.
     */
    public static final Predicate<String> MATCH_PREDICATE = Pattern.compile(EMAIL_REGEXP_FULL_LINE).asMatchPredicate();

}
