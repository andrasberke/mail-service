package com.aberke.mailservice.api.validation;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static com.aberke.mailservice.api.validation.EmailAddressPattern.EMAIL_REGEXP;

public class NameAndEmailAddressPattern {

    /**
     * Regexp for matching name and e-mail address in the form of
     * "John Smith <john.smith@example.com>"
      */
    private static final String REGEXP_FOR_VALIDATOR = "^([^<>]+)<" + EMAIL_REGEXP + ">$";
    /**
     * Predicate for matching strings using the name and email validation regexp.
     */
    public static final Predicate<String> MATCH_PREDICATE = Pattern.compile(REGEXP_FOR_VALIDATOR).asMatchPredicate();

    /**
     * Regexp for extracting name and email from "John Smith <john.smith@example.com>".
     * Does not validate e-mail address, it is only used for parsing.
     */
    private static final String REGEXP_FOR_PARSER = "^([^<>]+)<(.*?)>$";

    /**
     * Pattern for extracting name and email from "John Smith <john.smith@example.com>".
     */
    public static final Pattern PARSER_PATTERN = Pattern.compile(REGEXP_FOR_PARSER);
}
