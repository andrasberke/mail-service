package com.aberke.mailservice.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Annotation for validating e-mail addresses, see {@link EmailAddressValidator}.
 */
@Documented
@Constraint(validatedBy = EmailAddressValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EmailAddress {
    boolean optional() default false;

    String message() default "Invalid e-mail address";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

