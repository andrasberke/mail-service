package com.aberke.mailservice.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Annotation for validating list of e-mail addresses, see {@link EmailAddressValidator}.
 */
@Documented
@Constraint(validatedBy = EmailAddressListValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EmailAddressList {
    boolean optional() default false;

    String message() default "Invalid e-mail address list";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
