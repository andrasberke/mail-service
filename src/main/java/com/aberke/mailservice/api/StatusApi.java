package com.aberke.mailservice.api;

import com.aberke.mailservice.api.data.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.aberke.mailservice.api.data.Response.apiResponse;
import static org.springframework.http.HttpStatus.OK;

/**
 * REST controller for the service health (home) endpoint.
 */
@RestController
public class StatusApi {

    @GetMapping("/")
    public ResponseEntity<ApiResponse> serviceStatus() {
        return apiResponse(OK, "mail-service is running");
    }

}
