package com.aberke.mailservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Configurational parameters for e-mail sending.
 */
@Configuration
public class MailSchedulerConfiguration {

    @Value("${com.aberke.mailservice.sender.retryBackoff}")
    private int retryBackoffPeriod;

    @Value("${com.aberke.mailservice.sender.maxRetry}")
    private int maximumRetryAttempts;

    @Value("${com.aberke.mailservice.sender.batchSize}")
    private int senderBatchSize;

    @Autowired
    private DateTimeProvider dateTimeProvider;

    public int getRetryBackoffPeriod() {
        return retryBackoffPeriod;
    }

    public int getMaximumRetryAttempts() {
        return maximumRetryAttempts;
    }

    public int getSenderBatchSize() {
        return senderBatchSize;
    }

    public DateTimeProvider getDateTimeProvider() {
        return dateTimeProvider;
    }
}
