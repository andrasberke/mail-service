package com.aberke.mailservice.configuration;

import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 * Abstraction over current time information.
 */
@Component
public class DateTimeProvider {

    public Instant now() {
        return Instant.now();
    }

}
