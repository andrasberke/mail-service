package com.aberke.mailservice.domain.bounces;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * Basic implementation of {@link BounceInformation}.
 */
@AllArgsConstructor
public class SimpleBounceInformation implements BounceInformation {

    private final String email;
    private final String reason;
    private final String status;

    @Override
    public long getCreated() {
        return 0;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getReason() {
        return reason;
    }

    @Override
    public String getStatus() {
        return status;
    }

    /**
     * Simple implementation of {@link BounceList}.
     */
    public static class SimpleBounceList implements BounceList {

        private final List<BounceInformation> entries;

        public SimpleBounceList(BounceInformation... entries) {
            this.entries = Arrays.asList(entries);
        }

        @Override
        public List<BounceInformation> getEntries() {
            return entries;
        }

        @Override
        public boolean isEmpty() {
            return entries.isEmpty();
        }
    }
}
