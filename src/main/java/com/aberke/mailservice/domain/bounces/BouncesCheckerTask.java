package com.aberke.mailservice.domain.bounces;

import com.aberke.mailservice.domain.sender.MailSenderApi;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled task for querying bounces (undeliverable emails).
 */
@Component
@ConditionalOnProperty("com.aberke.mailservice.bounces.enabled")
@AllArgsConstructor
@Slf4j
public class BouncesCheckerTask {

    private final MailSenderApi senderApi;
    private final BounceHandler bounceHandler;

    /**
     * Called by the framework periodically.
     * 
     * Checks for bounces, passes to BounceHandler for processing.
     */
    @Scheduled(fixedRateString = "${com.aberke.mailservice.bounces.taskPeriod}")
    public void checkBounces() {
        log.debug("run");
        try {
            BounceList bounces = senderApi.queryBounces();
            if (!bounces.isEmpty()) {
                bounceHandler.handle(bounces);
                // After scheduled notification e-mails, the existing bounce events can be removed
                deleteBounces(bounces);
            }
        } catch (Exception e) {
            log.error("Unable to query bounces", e);
        }
    }

    private void deleteBounces(BounceList bounces) {
        try {
            senderApi.deleteBounces(bounces);
        } catch (Exception e) {
            log.error("Error while deleting bounces", e);
        }
    }

}
