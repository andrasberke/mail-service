package com.aberke.mailservice.domain.bounces;

import org.springframework.lang.NonNull;

/**
 * E-mail bounce event data provided by the external mail API.
 * 
 * Contains information about the recipient and the reason for the failure.
 */
public interface BounceInformation {

    long getCreated();

    @NonNull
    String getEmail();

    @NonNull
    String getReason();

    @NonNull
    String getStatus();

}
