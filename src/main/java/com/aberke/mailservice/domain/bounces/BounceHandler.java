package com.aberke.mailservice.domain.bounces;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.SentMail;
import com.aberke.mailservice.domain.OutboxService;
import com.aberke.mailservice.domain.notification.MailSchedulerNotificationsService;
import com.aberke.mailservice.domain.sender.MailSenderLog;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Responsible for handling bounces.
 * 
 * Rolls back transaction in case of failing to complete the task.
 */
@Component
@AllArgsConstructor
@Slf4j
public class BounceHandler {

    private final MailSenderLog senderLog;
    private final OutboxService outbox;
    private final MailSchedulerNotificationsService notificationsService;

    /**
     * Handles bounce events for the recipient e-mail addresses.
     *
     * @param bounces List of bounces received from the external API.
     */
    @Transactional
    public void handle(BounceList bounces) {
        bounces.getEntries().forEach(this::handle);
    }

    /**
     * Handling bounce event of single recipient.
     * 
     * Finds associated e-mail messages in the sent log,
     * notifies sender of the failure, marks messages 'undeliverable'.
     *
     * @param bounceInformation Bounce information (e-mail address, status)
     */
    private void handle(BounceInformation bounceInformation) {
        log.debug("Handling bounce {}", bounceInformation);
        List<SentMail> sentMails = senderLog.findBouncedMails(bounceInformation);
        log.debug("Found {} associated records in the log", sentMails.size());
        sentMails.forEach(mail -> handle(bounceInformation, mail));
    }

    private void handle(BounceInformation bounceInformation, SentMail sentMail) {
        // Note: in case of non-permanent bounces (e.g. mailbox full)
        // e-mail could be rescheduled instead of immediately notifying sender.
        sentMail.setNotifiedUndeliverable(true);
        Optional<OutboxMail> notificationEmail = notificationsService.notifyBounced(bounceInformation, sentMail);
        notificationEmail.ifPresent(outbox::add);
    }
}
