package com.aberke.mailservice.domain.bounces;

import java.util.List;

/**
 * E-mail bounces provided by the external mail API.
 * 
 * One {@link BounceInformation} entry represents one failed recipient in the log.
 */
public interface BounceList {

    List<? extends BounceInformation> getEntries();

    boolean isEmpty();
}
