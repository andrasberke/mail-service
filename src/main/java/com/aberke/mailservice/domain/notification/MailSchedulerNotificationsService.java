package com.aberke.mailservice.domain.notification;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailBuilder;
import com.aberke.mailservice.data.SentMail;
import com.aberke.mailservice.domain.bounces.BounceInformation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.aberke.mailservice.domain.notification.NotificationEmailTemplate.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.StringUtils.collectionToCommaDelimitedString;

/**
 * Service responsible for creating notification e-mails of Mail Service events (e.g. undeliverable e-mails)
 * for the users of the application.
 */
@Service
@Slf4j
public class MailSchedulerNotificationsService {

    public Optional<OutboxMail> notifyUndeliverable(OutboxMail mail) {

        if (NOTIFICATION_SENDER.equals(mail.getSender())) {
            // Do not notify if the original sender was the notification service
            return Optional.empty();
        }

        log.debug("Notify undeliverable {}", mail);

        // todo for demo only, should be external (localized) template
        OutboxMail notificationEmail = new OutboxMailBuilder()
                .setSender(NOTIFICATION_SENDER)
                .setTo(mail.getSender())
                .setSubject(String.format(NOTIFICATION_SUBJECT, mail.getSubject()))
                .setBody(String.format(NOTIFICATION_BODY, listRecipients(mail), NOTIFICATION_REASON_API, mail.getBody()))
                .build();

        return Optional.of(notificationEmail);
    }

    private String listRecipients(OutboxMail mail) {
        List<String> recipients = Stream.of(mail.getTo(), mail.getCc(), mail.getBcc())
                .filter(s -> !StringUtils.isEmpty(s))
                .collect(toList());
        return collectionToCommaDelimitedString(recipients);
    }

    public Optional<OutboxMail> notifyBounced(BounceInformation bounceInformation, SentMail mail) {

        if (NOTIFICATION_SENDER.equals(mail.getSender())) {
            // Do not notify if the original sender was the notification service
            return Optional.empty();
        }

        log.debug("Notify bounced {}", mail);

        // todo for demo only, should be external (localized) template
        String bounceReason = String.format(NOTIFICATION_REASON_BOUNCE, bounceInformation.getReason());
        String notificationSubject = String.format(NOTIFICATION_SUBJECT, mail.getSubject());
        String notificationBody = String.format(NOTIFICATION_BODY, bounceInformation.getEmail(), bounceReason, mail.getBody());
        OutboxMail notificationEmail = new OutboxMailBuilder()
                .setSender(NOTIFICATION_SENDER)
                .setTo(mail.getSender())
                .setSubject(notificationSubject)
                .setBody(notificationBody)
                .build();

        return Optional.of(notificationEmail);
    }
}
