package com.aberke.mailservice.domain.notification;

public class NotificationEmailTemplate {

    // todo for demo only, should be external (localized) template
    static final String NOTIFICATION_SENDER = "noreply@randomlongitude.com";
    static final String NOTIFICATION_SUBJECT = "Undeliverable email - %s";
    static final String NOTIFICATION_BODY = "Your email could not be delivered. \nRecipients: %s \n\nReason: %s \n\n======\n%s\n";

    static final String NOTIFICATION_REASON_API = "Error while calling mail sender API";
    static final String NOTIFICATION_REASON_BOUNCE = "Bounced - %s";

}
