package com.aberke.mailservice.domain.sender;

/**
 * Exception indicating an error while executing the mail send operation.
 * 
 * E.g. external mail service returning error response.
 */
public class MailSenderApiException extends RuntimeException {

    public MailSenderApiException(String message) {
        super(message);
    }

    public MailSenderApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailSenderApiException(Throwable cause) {
        super(cause);
    }

    public MailSenderApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
