package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.SentMail;
import com.aberke.mailservice.data.SentMailConverter;
import com.aberke.mailservice.data.SentMailRepository;
import com.aberke.mailservice.domain.bounces.BounceInformation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

/**
 * Responsible for logging sent emails.
 */
@Component
@AllArgsConstructor
@Slf4j
public class MailSenderLog {

    private final SentMailRepository repository;
    private final SentMailConverter sentMailConverter;
    private final DateTimeProvider dateTimeProvider;

    public void addSentEmail(OutboxMail mail) {
        Instant sentAt = dateTimeProvider.now();
        List<SentMail> sentMails = sentMailConverter.convert(mail, sentAt);
        repository.saveAll(sentMails);
        log.debug("Adding {} sent mails to log", sentMails.size());
    }

    public List<SentMail> findBouncedMails(BounceInformation bounceInformation) {
        // Note: query is not checking for bounce event creation time.
        // Based on the BounceInformation.getCreated() value it should be possible
        // to find the corresponding e-mail(s) in the log.
        // However, for simple cases (e.g. invalid e-mail address) returning all e-mails
        // that are not marked as 'undelivered' yet is already sufficient.
        return repository.findSentMailsOfRecipient(bounceInformation.getEmail());
    }
}
