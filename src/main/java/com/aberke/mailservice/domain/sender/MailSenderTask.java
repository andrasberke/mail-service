package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.domain.OutboxService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduled task for sending out e-mails.
 */
@Component
@ConditionalOnProperty("com.aberke.mailservice.sender.enabled")
@AllArgsConstructor
@Slf4j
public class MailSenderTask {

    private final OutboxService outbox;
    private final MailSender sender;

    /**
     * Called by the framework periodically.
     * Reads the next batch of pending e-mails from the database and sends them via the external API.
     *
     * Successfully sent e-mails are removed from the outbox.
     * Failed e-mails are being rescheduled for retry.
     */
    @Scheduled(fixedRateString = "${com.aberke.mailservice.sender.taskPeriod}")
    public void sendEmails() {
        log.debug("run");
        outbox.getPendingEmails().forEach(this::sendEmail);
    }

    private void sendEmail(OutboxMail mail) {
        try {
            sender.attemptSending(mail);
            log.debug("Successfully sent and removed from Outbox {}", mail);
        } catch (MailSenderApiException senderException) {
            log.debug("Sender API exception while sending {}", mail);
            log.debug("Stack trace is ", senderException);
            outbox.reschedule(mail);
        } catch (Exception e) {
            log.error("Unhandled exception while sending", e);
        }
    }

}

