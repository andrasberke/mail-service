package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.domain.OutboxService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Responsible for executing e-mail sending tasks.
 * 
 * Rolls back transaction in case of sending failure.
 */
@Component
@AllArgsConstructor
public class MailSender {

    private final OutboxService outbox;
    private final MailSenderApi senderApi;
    private final MailSenderLog senderLog;

    @Transactional
    public void attemptSending(OutboxMail mail) {
        outbox.remove(mail);
        senderLog.addSentEmail(mail);
        senderApi.sendMail(mail);
    }
}
