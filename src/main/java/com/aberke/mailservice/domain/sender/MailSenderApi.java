package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.data.Mail;
import com.aberke.mailservice.domain.bounces.BounceList;

/**
 * Interface for the external e-mail sender API.
 */
public interface MailSenderApi {

    /**
     * Calls external API to send an e-mail.
     *
     * @param mail Domain object, email to send.
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    void sendMail(Mail mail) throws MailSenderApiException;

    /**
     * Calls external API to query undelivered e-mails.
     *
     * @return List of bounces returned by the mailer API.
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    BounceList queryBounces() throws MailSenderApiException;

    /**
     * Calls external API to delete all bounce events.
     *
     * @param bounces List of entries to remove.
     * @throws MailSenderApiException In case of the request could not be completed (e.g. error response received).
     */
    void deleteBounces(BounceList bounces) throws MailSenderApiException;
}
