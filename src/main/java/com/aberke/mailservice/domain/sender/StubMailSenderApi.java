package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.data.Mail;
import com.aberke.mailservice.domain.bounces.BounceList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import static com.aberke.mailservice.domain.bounces.SimpleBounceInformation.SimpleBounceList;

/**
 * Stub implementation of {@link MailSenderApi} for testing purposes.
 * 
 * Only logs the received {@link Mail} parameter.
 */
@Service
@ConditionalOnProperty(name = "com.aberke.mailservice.provider", havingValue = "stub")
@Slf4j
public class StubMailSenderApi implements MailSenderApi {

    @Value(value = "${com.aberke.mailservice.provider.stub.enabled}")
    private boolean enabled;

    @Override
    public void sendMail(Mail mail) throws MailSenderApiException {
        log.debug("STUB Sending mail {}", mail);
        throwIfDisabled();
    }

    private void throwIfDisabled() {
        if (!enabled) {
            throw new MailSenderApiException("STUB Sender is disabled");
        }
    }

    @Override
    public BounceList queryBounces() throws MailSenderApiException {
        log.debug("STUB Query bounces");
        return new SimpleBounceList();
    }

    @Override
    public void deleteBounces(BounceList bounces) throws MailSenderApiException {
        log.debug("STUB Delete bounces");
    }

}
