package com.aberke.mailservice.domain;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.configuration.MailSchedulerConfiguration;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.aberke.mailservice.domain.notification.MailSchedulerNotificationsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service for managing the outbox.
 * 
 * Allows clients to add / remove e-mails, fetch the next scheduled batch and reschedule failed e-mails.
 */
@Service
@Slf4j
public class OutboxService {

    private final OutboxMailRepository repository;
    private final DateTimeProvider dateTimeProvider;
    private final MailSchedulerNotificationsService notificationsService;

    private final int retryBackoffPeriod;
    private final int maximumRetryAttempts;
    private final int emailSenderBatchSize;

    public OutboxService(
            OutboxMailRepository repository,
            MailSchedulerNotificationsService notificationsService,
            MailSchedulerConfiguration configuration) {
        this.repository = repository;
        this.notificationsService = notificationsService;
        this.retryBackoffPeriod = configuration.getRetryBackoffPeriod();
        this.maximumRetryAttempts = configuration.getMaximumRetryAttempts();
        this.emailSenderBatchSize = configuration.getSenderBatchSize();
        this.dateTimeProvider = configuration.getDateTimeProvider();
    }

    public void add(OutboxMail mail) {
        // Schedule for immediate delivery
        mail.setRetryCount(0);
        mail.setScheduledTimestamp(dateTimeProvider.now().toEpochMilli());
        repository.save(mail);
        log.debug("Added {}", mail);
    }

    public void remove(OutboxMail mail) {
        repository.delete(mail);
        log.debug("Deleted {}", mail);
    }

    /**
     * Returning the next batch of e-mails in the outbox scheduled for delivery.
     *
     * @return List of pending e-mails to send.
     */
    public List<OutboxMail> getPendingEmails() {
        return repository.getNextBatch(emailSenderBatchSize, dateTimeProvider.now());
    }

    /**
     * Reschedule failed e-mail.
     * 
     * Retry count will be increased, if it reached the max value the mail will be marked as undeliverable.
     * Undeliverable e-mails are sent to the notification service for error handling.
     *
     * @param mail E-mail that the service failed to send.
     */
    public void reschedule(OutboxMail mail) {

        mail.incrementRetryCount();
        if (mail.getRetryCount() <= maximumRetryAttempts) {
            mail.delayScheduledTime(retryBackoffPeriod);
            log.debug("Rescheduling {}", mail);
        } else {
            log.debug("Reached max retry count for {}", mail);
            mail.setUndeliverable(true);
        }
        repository.save(mail);

        if (mail.isUndeliverable()) {
            notifyUndeliverable(mail);
        }
    }

    private void notifyUndeliverable(OutboxMail mail) {
        Optional<OutboxMail> undeliverableEmailNotification = notificationsService.notifyUndeliverable(mail);
        undeliverableEmailNotification.ifPresent(this::add);
    }

}
