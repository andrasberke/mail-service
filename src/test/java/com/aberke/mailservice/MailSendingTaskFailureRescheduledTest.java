package com.aberke.mailservice;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailBuilder;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.aberke.mailservice.data.SentMailRepository;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import com.aberke.mailservice.domain.sender.MailSenderApiException;
import org.awaitility.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

@DirtiesContext
@SpringBootTest(properties = {
        "com.aberke.mailservice.sender.enabled=true",
        "com.aberke.mailservice.sender.taskPeriod=100",
        "com.aberke.mailservice.sender.retryBackoff=60000",
        "com.aberke.mailservice.bounces.enabled=false"
})
public class MailSendingTaskFailureRescheduledTest {

    private static final long TEST_RETRY_BACKOFF = 60000;

    @TestConfiguration
    public static class TestConfig {
        @Bean
        @Primary
        public DateTimeProvider mockTime() {
            DateTimeProvider mockTime = mock(DateTimeProvider.class);
            when(mockTime.now()).thenReturn(Instant.ofEpochMilli(500));
            return mockTime;
        }
        @Bean
        @Primary
        public MailSenderApi mailSenderApi() {
            MailSenderApi mailSenderApi = mock(MailSenderApi.class);
            doThrow(MailSenderApiException.class).when(mailSenderApi).sendMail(any());
            return mailSenderApi;
        }
    }

    @Autowired
    private OutboxMailRepository outboxRepository;
    @Autowired
    private SentMailRepository sentMailRepository;

    private OutboxMail scheduledMail;

    @BeforeEach
    public void initTestData() {
        scheduledMail = new OutboxMailBuilder()
                .setSender("from@example.com")
                .setTo("to1@example.com,to2@example.com")
                .setCc("cc1@exmaple.com,cc2@exampple.com")
                .setBcc("bcc1@example.com,bcc2@example.com")
                .setSubject("test subject")
                .setBody("test body")
                .build();
        outboxRepository.save(scheduledMail);
    }

    @Test
    public void givenOutboxContainsMail_whenApiFails_thenMailIsRescheduled() {

        // Wait for task completion
        await().atMost(Duration.ONE_SECOND)
                .untilAsserted(() -> {
                    assertThat(outboxRepository.findAll()).first().extracting("scheduledTimestamp").isEqualTo(TEST_RETRY_BACKOFF);
                });

        // Validate saved outbox email
        scheduledMail.setId(1);
        scheduledMail.setScheduledTimestamp(TEST_RETRY_BACKOFF);
        scheduledMail.setRetryCount(1);
        assertThat(outboxRepository.findAll()).usingFieldByFieldElementComparator().containsOnly(scheduledMail);
        assertThat(sentMailRepository.findAll()).isEmpty();
    }

}
