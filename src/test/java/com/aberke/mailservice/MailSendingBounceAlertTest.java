package com.aberke.mailservice;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.aberke.mailservice.data.SentMail;
import com.aberke.mailservice.data.SentMailRepository;
import com.aberke.mailservice.domain.bounces.BounceInformation;
import com.aberke.mailservice.domain.bounces.BounceList;
import com.aberke.mailservice.domain.bounces.SimpleBounceInformation;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import org.awaitility.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;

import static com.aberke.mailservice.domain.bounces.SimpleBounceInformation.SimpleBounceList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@DirtiesContext
@SpringBootTest(properties = {
        "com.aberke.mailservice.sender.enabled=false",
        "com.aberke.mailservice.bounces.enabled=true",
        "com.aberke.mailservice.bounces.taskPeriod=100"
})
public class MailSendingBounceAlertTest {
    
    @TestConfiguration
    public static class TestConfig {
        @Bean
        @Primary
        public DateTimeProvider mockTime() {
            DateTimeProvider mockTime = mock(DateTimeProvider.class);
            when(mockTime.now()).thenReturn(Instant.ofEpochMilli(500));
            return mockTime;
        }
        @Bean
        @Primary
        public MailSenderApi mailSenderApi() {
            MailSenderApi mailSenderApi = mock(MailSenderApi.class);
            BounceInformation testBounce = new SimpleBounceInformation("to@fake.com", "reason", "status");
            BounceList bounceList = new SimpleBounceList(testBounce);
            when(mailSenderApi.queryBounces()).thenReturn(bounceList);
            return mailSenderApi;
        }
    }

    @Autowired
    private OutboxMailRepository outboxRepository;
    @Autowired
    private SentMailRepository sentMailRepository;

    private SentMail sentMail;

    @BeforeEach
    public void initTestData() {
        sentMail = new SentMail();
        sentMail.setSender("from@example.com");
        sentMail.setRecipient("to@fake.com");
        sentMail.setRecipientType(SentMail.RecipientType.TO);
        sentMail.setSubject("subject");
        sentMail.setBody("body");
        sentMailRepository.save(sentMail);
    }

    @Test
    public void givenSentMail_whenApiReturnsBounceEvent_thenBounceAlertIsScheduled() {

        // Wait for task completion
        await().atMost(Duration.ONE_SECOND)
                .untilAsserted(() -> {
                    assertThat(outboxRepository.findAll()).hasSize(1);
                });

        // Validate that fail alert email has been scheduled
        OutboxMail failAlertEmail = outboxRepository.findById(1).get();
        assertThat(failAlertEmail).extracting("to").isEqualTo("from@example.com");
        // Validate that sent email is marked as undeliverable
        sentMail.setNotifiedUndeliverable(true);
        SentMail undeliverableMail = sentMailRepository.findById(1).get();
        assertThat(undeliverableMail).isEqualToComparingFieldByField(sentMail);
    }
}

