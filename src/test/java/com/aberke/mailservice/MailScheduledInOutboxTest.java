package com.aberke.mailservice;

import com.aberke.mailservice.api.data.MailRequest;
import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailBuilder;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@SpringBootTest(properties = {
        "com.aberke.mailservice.sender.enabled=false",
        "com.aberke.mailservice.bounces.enabled=false"
})
class MailScheduledInOutboxTest {

    private static final long TEST_TS = 500;

    @Autowired
    private WebApplicationContext applicationContext;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private OutboxMailRepository repository;
    @MockBean
    private DateTimeProvider dateTimeProvider;

    private MockMvc mockMvc;
    private MailRequest testRequest;
    private OutboxMail expectedMail;

    @BeforeEach
    public void initTestData() {
        testRequest = new MailRequest();
        testRequest.setFrom("from@example.com");
        testRequest.setTo("to1@example.com,to2@example.com");
        testRequest.setCc("cc1@exmaple.com,cc2@exampple.com");
        testRequest.setBcc("bcc1@example.com,bcc2@example.com");
        testRequest.setSubject("test subject");
        testRequest.setBody("test body");
        expectedMail = new OutboxMailBuilder()
                .setSender("from@example.com")
                .setTo("to1@example.com,to2@example.com")
                .setCc("cc1@exmaple.com,cc2@exampple.com")
                .setBcc("bcc1@example.com,bcc2@example.com")
                .setSubject("test subject")
                .setBody("test body")
                .build();
        expectedMail.setId(1);
        expectedMail.setScheduledTimestamp(TEST_TS);
        when(dateTimeProvider.now()).thenReturn(Instant.ofEpochMilli(TEST_TS));
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    private ResultActions performPost(MailRequest request) throws Exception {
        return mockMvc.perform(post("/send")
                .header("Authorization", "Basic dXNlcjpwYXNzd29yZA==")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));
    }

    @Test
    void whenSubmittingValidRequest_thenMailIsScheduledInOutbox() throws Exception {
        // WHEN
        performPost(testRequest).andExpect(status().isOk());
        Iterable<OutboxMail> inserted = repository.findAll();
        // THEN
        assertThat(inserted).usingFieldByFieldElementComparator().containsOnly(expectedMail);
    }

}
