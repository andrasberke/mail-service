package com.aberke.mailservice.domain.sender;

import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.domain.OutboxService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MailSenderTaskTest {

    @Mock
    private OutboxService outboxService;
    @Mock
    private MailSender mailSender;
    @Mock
    private OutboxMail mail;

    private MailSenderTask senderTask;

    @BeforeEach
    public void init() {
        senderTask = new MailSenderTask(outboxService, mailSender);
    }

    @Test
    public void givenMailInOutbox_whenTaskIsTriggered_thenMailIsSent() {
        when(outboxService.getPendingEmails()).thenReturn(List.of(mail));
        // WHEN
        senderTask.sendEmails();
        // THEN
        verify(mailSender).attemptSending(mail);
    }

    @Test
    public void givenMailInOutboxFailedToSend_whenTaskIsTriggered_thenMailIsRescheduled() {
        when(outboxService.getPendingEmails()).thenReturn(List.of(mail));
        doThrow(MailSenderApiException.class).when(mailSender).attemptSending(mail);
        // WHEN
        senderTask.sendEmails();
        // THEN
        verify(outboxService).reschedule(mail);
    }

    @Test
    public void givenMailInOutboxFailedWithUnknownException_whenTaskIsTriggered_noop() {
        when(outboxService.getPendingEmails()).thenReturn(List.of(mail));
        doThrow(RuntimeException.class).when(mailSender).attemptSending(mail);
        // WHEN
        senderTask.sendEmails();
        // THEN
        verify(outboxService, never()).reschedule(any());
    }
}
