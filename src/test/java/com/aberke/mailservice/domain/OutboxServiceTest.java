package com.aberke.mailservice.domain;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.configuration.MailSchedulerConfiguration;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.aberke.mailservice.domain.notification.MailSchedulerNotificationsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OutboxServiceTest {

    private static final Instant TEST_TS = Instant.ofEpochMilli(500);
    private static final int TEST_BACKOFF_PERIOD = 1000;
    private static final int TEST_MAX_RETRY = 1;
    private static final int TEST_BATCH_SIZE = 10;

    @Mock
    private OutboxMailRepository repository;
    @Mock
    private MailSchedulerNotificationsService notificationsService;
    @Mock
    private MailSchedulerConfiguration configuration;
    @Mock
    private DateTimeProvider dateTimeProvider;
    @Mock
    private OutboxMail mail;

    private OutboxService outboxService;

    @BeforeEach
    public void init() {
        when(configuration.getDateTimeProvider()).thenReturn(dateTimeProvider);
        when(configuration.getRetryBackoffPeriod()).thenReturn(TEST_BACKOFF_PERIOD);
        when(configuration.getMaximumRetryAttempts()).thenReturn(TEST_MAX_RETRY);
        when(configuration.getSenderBatchSize()).thenReturn(TEST_BATCH_SIZE);
        outboxService = new OutboxService(repository, notificationsService, configuration);
    }

    @Test
    public void givenMail_whenAddingToOutbox_thenMailIsScheduledForDelivery() {
        when(dateTimeProvider.now()).thenReturn(TEST_TS);
        // WHEN
        outboxService.add(mail);
        // THEN
        verify(mail).setRetryCount(0);
        verify(mail).setScheduledTimestamp(eq(TEST_TS.toEpochMilli()));
        verify(repository).save(eq(mail));
    }

    @Test
    public void givenMail_whenRemovingFromOutbox_thenMailIsRemovedFromRepository() {
        // WHEN
        outboxService.remove(mail);
        // THEN
        verify(repository).delete(eq(mail));
    }

    @Test
    public void whenGettingEmailsToSend_thenRepositoryIsQueried() {
        when(dateTimeProvider.now()).thenReturn(TEST_TS);
        when(repository.getNextBatch(TEST_BATCH_SIZE, TEST_TS)).thenReturn(List.of(mail));
        // WHEN
        List<OutboxMail> mails = outboxService.getPendingEmails();
        // THEN
        assertThat(mails).containsExactly(mail);
    }

    @Test
    public void givenFailedEmail_whenRescheduling_thenMailIsSavedForLater() {
        when(mail.getRetryCount()).thenReturn(0);
        // WHEN
        outboxService.reschedule(mail);
        // THEN
        verify(mail).incrementRetryCount();
        verify(mail).delayScheduledTime(TEST_BACKOFF_PERIOD);
        verify(repository).save(mail);
    }

    @Test
    public void givenPermanentlyFailedEmail_whenRescheduling_thenMailIsSetAsUndeliverableWithNotification() {
        when(mail.getRetryCount()).thenReturn(TEST_MAX_RETRY + 1);
        when(mail.isUndeliverable()).thenReturn(true);
        when(notificationsService.notifyUndeliverable(eq(mail))).thenReturn(Optional.empty());
        // WHEN
        outboxService.reschedule(mail);
        // THEN
        verify(mail).setUndeliverable(true);
        verify(repository).save(mail);
        verify(notificationsService).notifyUndeliverable(eq(mail));
    }
}
