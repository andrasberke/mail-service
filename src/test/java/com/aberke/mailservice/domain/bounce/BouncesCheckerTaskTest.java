package com.aberke.mailservice.domain.bounce;

import com.aberke.mailservice.domain.bounces.*;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.aberke.mailservice.domain.bounces.SimpleBounceInformation.SimpleBounceList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BouncesCheckerTaskTest {

    @Mock
    private MailSenderApi mailSenderApi;
    @Mock
    private BounceHandler bounceHandler;

    private BounceInformation testBounce;
    private BounceList testBounceList;

    private BouncesCheckerTask checkerTask;

    @BeforeEach
    public void init() {
        testBounce = new SimpleBounceInformation("email", "reason", "status");
        testBounceList = new SimpleBounceList(testBounce);
        checkerTask = new BouncesCheckerTask(mailSenderApi, bounceHandler);
    }

    @Test
    public void givenEmptyBounceList_whenTaskIsTriggered_noop() {
        when(mailSenderApi.queryBounces()).thenReturn(new SimpleBounceList());
        // WHEN
        checkerTask.checkBounces();
        // THEN
        verify(mailSenderApi, never()).deleteBounces(any());
    }

    @Test
    public void givenBounceEntry_whenTaskIsTriggered_thenHandlerIsCalled() {
        when(mailSenderApi.queryBounces()).thenReturn(testBounceList);
        // WHEN
        checkerTask.checkBounces();
        // THEN
        verify(bounceHandler).handle(testBounceList);
        verify(mailSenderApi).deleteBounces(testBounceList);
    }

    @Test
    public void givenBounceEntryFailing_whenTaskIsTriggered_thenBouncesAreNotRemoved() {
        when(mailSenderApi.queryBounces()).thenReturn(testBounceList);
        doThrow(RuntimeException.class).when(bounceHandler).handle(testBounceList);
        // WHEN
        checkerTask.checkBounces();
        // THEN
        verify(mailSenderApi, never()).deleteBounces(any());
    }
}
