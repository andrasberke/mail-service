package com.aberke.mailservice;

import com.aberke.mailservice.api.EmailApi;
import com.aberke.mailservice.api.data.MailRequest;
import com.aberke.mailservice.api.data.MailRequestConverter;
import com.aberke.mailservice.domain.OutboxService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = EmailApi.class)
public class EmailApiValidationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private MailRequestConverter mailRequestConverter;
    @MockBean
    private OutboxService outbox;

    private MailRequest testRequest;

    @BeforeEach
    public void initTestRequest() {
        testRequest = new MailRequest();
        testRequest.setFrom("from@example.com");
        testRequest.setTo("to1@example.com,to2@example.com");
        testRequest.setCc("cc1@exmaple.com,cc2@exampple.com");
        testRequest.setBcc("bcc1@example.com,bcc2@example.com");
        testRequest.setSubject("test subject");
        testRequest.setBody("test body");
    }

    private ResultActions performPost(MailRequest request) throws Exception {
        return mockMvc.perform(post("/send")
                .header("Authorization", "Basic dXNlcjpwYXNzd29yZA==")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));
    }

    @Test
    public void whenRequestIsValid_thenReturnOK() throws Exception {
        performPost(testRequest).andExpect(status().isOk());
    }

    @Test
    public void whenSenderEmailAddressIsInvalid_thenReturnValidationError() throws Exception {
        testRequest.setFrom("abbb@");
        performPost(testRequest).andExpect(status().isBadRequest());
        verifyNoInteractions(outbox);
    }

    @Test
    public void whenRecipientEmailAddressIsInvalid_thenReturnValidationError() throws Exception {
        testRequest.setTo("to1@example.com,to2@");
        performPost(testRequest).andExpect(status().isBadRequest());
        verifyNoInteractions(outbox);
    }

    @Test
    public void whenCcRecipientEmailAddressIsInvalid_thenReturnValidationError() throws Exception {
        testRequest.setCc("cc1@example.com,cccc");
        performPost(testRequest).andExpect(status().isBadRequest());
        verifyNoInteractions(outbox);
    }

    @Test
    public void whenBccRecipientEmailAddressIsInvalid_thenReturnValidationError() throws Exception {
        testRequest.setBcc(",");
        performPost(testRequest).andExpect(status().isBadRequest());
        verifyNoInteractions(outbox);
    }
}
