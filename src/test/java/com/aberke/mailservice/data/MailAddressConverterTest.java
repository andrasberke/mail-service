package com.aberke.mailservice.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MailAddressConverterTest {

    private static final String TEST_NAME = "John Smith";
    private static final String TEST_ADDRESS = "john.smith@example.com";
    private static final String TEST_NAME_AND_ADDRESS = "John Smith <john.smith@example.com>";
    private static final String TEST_ADDRESS_2 = "abc123@example.com";

    private static final List<String> ADDRESS_LIST = Arrays.asList(TEST_NAME_AND_ADDRESS, TEST_ADDRESS_2);
    private static final String ADDRESS_LIST_CSV = StringUtils.collectionToCommaDelimitedString(ADDRESS_LIST);

    private MailAddressConverter parser;

    @BeforeEach
    public void init() {
        // GIVEN
        parser = new MailAddressConverter();
    }

    @Test
    public void givenStandaloneEmailAddress_whenCallingParser_thenReturnEntryWithNullName() {
        // WHEN
        MailAddress addressEntry = parser.convertAddress(TEST_ADDRESS);
        // THEN
        assertThat(addressEntry.getEmailAddress()).isEqualTo(TEST_ADDRESS);
        assertThat(addressEntry.getName()).isNull();
    }

    @Test
    public void givenNameAndEmailAddress_whenCallingParser_thenReturnEntryWithName() {
        // WHEN
        MailAddress addressEntry = parser.convertAddress(TEST_NAME_AND_ADDRESS);
        // THEN
        assertThat(addressEntry.getEmailAddress()).isEqualTo(TEST_ADDRESS);
        assertThat(addressEntry.getName()).isEqualTo(TEST_NAME);
    }

    @Test
    public void givenListOfEmailAddresses_whenCallingParser_thenReturnAllEntries() {
        // WHEN
        List<MailAddress> addressEntries = parser.convertAddressList(ADDRESS_LIST_CSV);
        // THEN
        assertThat(addressEntries).hasSize(2);
        MailAddress entry1 = addressEntries.get(0);
        MailAddress entry2 = addressEntries.get(1);
        assertThat(entry1.getEmailAddress()).isEqualTo(TEST_ADDRESS);
        assertThat(entry1.getName()).isEqualTo(TEST_NAME);
        assertThat(entry2.getEmailAddress()).isEqualTo(TEST_ADDRESS_2);
        assertThat(entry2.getName()).isNull();
    }
}
