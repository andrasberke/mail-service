package com.aberke.mailservice;

import com.aberke.mailservice.data.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class DatabaseIntegrationTest {

    private static final int TEST_BATCH_SIZE = 100;
    private static final Instant TEST_TS = Instant.ofEpochMilli(50);
    private static final Instant TEST_WINDOW_END = Instant.ofEpochMilli(100);

    @Autowired
    private OutboxMailRepository outboxMailRepository;
    @Autowired
    private SentMailRepository sentMailRepository;

    @Test
    public void testOutboxJpaOperations() {

        // WHEN
        List<OutboxMail> emptyOutbox = outboxMailRepository.getNextBatch(TEST_BATCH_SIZE, TEST_WINDOW_END);
        OutboxMail savedMail = new OutboxMailBuilder()
                .setSender("from")
                .setTo("to")
                .setCc("cc")
                .setBcc("bcc")
                .setSubject("subject")
                .setBody("body")
                .build();
        savedMail.setScheduledTimestamp(TEST_TS.toEpochMilli());
        outboxMailRepository.save(savedMail);
        List<OutboxMail> readOutbox = outboxMailRepository.getNextBatch(TEST_BATCH_SIZE, TEST_WINDOW_END);
        outboxMailRepository.delete(savedMail);
        List<OutboxMail> clearOutbox = outboxMailRepository.getNextBatch(TEST_BATCH_SIZE, TEST_WINDOW_END);

        // THEN
        assertThat(emptyOutbox).isEmpty();
        assertThat(savedMail.getId()).isPositive();
        assertThat(readOutbox).hasSize(1);
        assertThat(readOutbox.get(0)).isEqualToComparingFieldByField(savedMail);
        assertThat(clearOutbox).isEmpty();

    }

    @Test
    public void testSentMailJpaOperations() {

        // WHEN
        List<SentMail> emptySentLog = sentMailRepository.findSentMailsOfRecipient("recipient");
        SentMail sentMail = new SentMail();
        sentMail.setSender("sender");
        sentMail.setRecipient("recipient");
        sentMail.setRecipientType(SentMail.RecipientType.TO);
        sentMail.setSentTimestamp(12345);
        sentMail.setSubject("subject");
        sentMail.setBody("body");
        sentMailRepository.save(sentMail);
        List<SentMail> readSentLog = sentMailRepository.findSentMailsOfRecipient("recipient");
        sentMailRepository.delete(sentMail);
        List<SentMail> clearSentLog = sentMailRepository.findSentMailsOfRecipient("recipient");

        // THEN
        assertThat(emptySentLog).isEmpty();
        assertThat(sentMail.getId()).isPositive();
        assertThat(readSentLog).hasSize(1);
        assertThat(readSentLog.get(0)).isEqualToComparingFieldByField(sentMail);
        assertThat(clearSentLog).isEmpty();

    }
}
