package com.aberke.mailservice.api.validation;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailAddressValidatorTest {

    @Test
    public void givenEmptyEmailAddress_whenValidatorCalled_thenReturnFailure() {
        String email = "";
        boolean result = new EmailAddressValidator().isValid(email, null);
        assertThat(result).isFalse();
    }

    @Test
    public void givenValidStandaloneEmailAddress_whenValidatorCalled_thenReturnSuccess() {
        String email = "john.smith@example.com";
        boolean result = new EmailAddressValidator().isValid(email, null);
        assertThat(result).isTrue();
    }

    @Test
    public void givenInvalidStandaloneEmailAddress_whenValidatorCalled_thenReturnFailure() {
        String email = "john.smith@";
        boolean result = new EmailAddressValidator().isValid(email, null);
        assertThat(result).isFalse();
    }

    @Test
    public void givenNameAndValidEmailAddress_whenValidatorCalled_thenReturnSuccess() {
        String email = "John Smith <john.smith@example.com>";
        boolean result = new EmailAddressValidator().isValid(email, null);
        assertThat(result).isTrue();
    }

    @Test
    public void givenNameAndInvalidEmailAddress_whenValidatorCalled_thenReturnFailure() {
        String email = "John Smith <john.smith@exampleeeee>";
        boolean result = new EmailAddressValidator().isValid(email, null);
        assertThat(result).isFalse();
    }
}
