package com.aberke.mailservice.api.validation;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailAddressListValidatorTest {

    @Test
    public void givenValidListOfEmails_whenValidatorCalled_thenReturnSuccess() {
        String emails = "to1@example.com, to2@example.com";
        boolean result = new EmailAddressListValidator().isValid(emails, null);
        assertThat(result).isTrue();
    }

    @Test
    public void givenValidMixedListOfEmails_whenValidatorCalled_thenReturnSuccess() {
        String emails = "to1@example.com, John Smith <to2@example.com>";
        boolean result = new EmailAddressListValidator().isValid(emails, null);
        assertThat(result).isTrue();
    }

    @Test
    public void givenListOfEmailsWithInvalidEntry_whenValidatorCalled_thenReturnFailure() {
        String emails = "to1@example.com, John Smith <to2@exampleeee>";
        boolean result = new EmailAddressListValidator().isValid(emails, null);
        assertThat(result).isFalse();
    }
}
