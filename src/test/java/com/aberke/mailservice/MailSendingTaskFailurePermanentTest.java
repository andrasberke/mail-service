package com.aberke.mailservice;

import com.aberke.mailservice.configuration.DateTimeProvider;
import com.aberke.mailservice.data.OutboxMail;
import com.aberke.mailservice.data.OutboxMailBuilder;
import com.aberke.mailservice.data.OutboxMailRepository;
import com.aberke.mailservice.data.SentMailRepository;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import com.aberke.mailservice.domain.sender.MailSenderApiException;
import org.awaitility.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@DirtiesContext
@SpringBootTest(properties = {
        "com.aberke.mailservice.sender.enabled=true",
        "com.aberke.mailservice.sender.taskPeriod=100",
        "com.aberke.mailservice.sender.maxRetry=5",
        "com.aberke.mailservice.bounces.enabled=false"
})
public class MailSendingTaskFailurePermanentTest {

    private static final int TEST_MAX_RETRY = 5;

    @TestConfiguration
    public static class TestConfig {
        @Bean
        @Primary
        public DateTimeProvider mockTime() {
            DateTimeProvider mockTime = mock(DateTimeProvider.class);
            when(mockTime.now()).thenReturn(Instant.ofEpochMilli(500));
            return mockTime;
        }
        @Bean
        @Primary
        public MailSenderApi mailSenderApi() {
            MailSenderApi mailSenderApi = mock(MailSenderApi.class);
            doThrow(MailSenderApiException.class).when(mailSenderApi).sendMail(any());
            return mailSenderApi;
        }
    }

    @Autowired
    private OutboxMailRepository outboxRepository;
    @Autowired
    private SentMailRepository sentMailRepository;

    private OutboxMail scheduledMail;

    @BeforeEach
    public void initTestData() {
        scheduledMail = new OutboxMailBuilder()
                .setSender("from@example.com")
                .setTo("to1@example.com,to2@example.com")
                .setCc("cc1@exmaple.com,cc2@exampple.com")
                .setBcc("bcc1@example.com,bcc2@example.com")
                .setSubject("test subject")
                .setBody("test body")
                .build();
        scheduledMail.setRetryCount(TEST_MAX_RETRY);
        outboxRepository.save(scheduledMail);
    }

    @Test
    public void givenOutboxContainsRetriedMail_whenApiFails_thenMailIsMarkedUndeliverable() {

        // Wait for task completion
        await().atMost(Duration.ONE_SECOND)
                .untilAsserted(() -> {
                    assertThat(outboxRepository.findAll()).hasSize(2);
                });

        // Validate that outbox email is marked as undeliverable
        scheduledMail.setRetryCount(TEST_MAX_RETRY + 1);
        scheduledMail.setUndeliverable(true);
        OutboxMail undeliveredMail = outboxRepository.findById(1).get();
        assertThat(undeliveredMail).isEqualToComparingFieldByField(scheduledMail);
        // Validate that fail alert email has been scheduled
        OutboxMail failAlertEmail = outboxRepository.findById(2).get();
        assertThat(failAlertEmail).extracting("to").isEqualTo("from@example.com");
    }
}

