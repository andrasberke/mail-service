package com.aberke.mailservice;

import com.aberke.mailservice.data.*;
import com.aberke.mailservice.domain.sender.MailSenderApi;
import org.awaitility.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.verify;

@DirtiesContext
@SpringBootTest(properties = {
        "com.aberke.mailservice.sender.enabled=true",
        "com.aberke.mailservice.sender.taskPeriod=100",
        "com.aberke.mailservice.bounces.enabled=false"
})
public class MailSendingTaskSuccessTest {

    @Autowired
    private OutboxMailRepository outboxRepository;
    @Autowired
    private SentMailRepository sentMailRepository;
    @MockBean
    private MailSenderApi mailSenderApi;

    private OutboxMail scheduledMail;

    @BeforeEach
    public void initTestData() {
        scheduledMail = new OutboxMailBuilder()
                .setSender("from@example.com")
                .setTo("to1@example.com,to2@example.com")
                .setCc("cc1@exmaple.com,cc2@exampple.com")
                .setBcc("bcc1@example.com,bcc2@example.com")
                .setSubject("test subject")
                .setBody("test body")
                .build();
        outboxRepository.save(scheduledMail);
    }

    @Test
    public void givenOutboxContainsMail_whenTaskIsRunning_thenSendApiIsCalledAndSentMailSaved() {

        // Wait for task completion
        await().atMost(Duration.ONE_SECOND)
                .untilAsserted(() -> {
                    assertThat(sentMailRepository.findAll()).hasSize(6);
                });

        // Validate mail sent via the API
        ArgumentCaptor<Mail> argumentCaptor = ArgumentCaptor.forClass(Mail.class);
        verify(mailSenderApi).sendMail(argumentCaptor.capture());
        Mail sentMail = argumentCaptor.getValue();
        assertThat(sentMail).isEqualToComparingFieldByField(scheduledMail);
    }
}
