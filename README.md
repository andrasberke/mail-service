# README

mailservice - Java Spring Boot demo service for sending e-mails.

# Aim

The application was developed as a coding exercise in 2020. 

The task was to create an e-mail sending service with [SendGrid](https://sendgrid.com/) integration, 
available on HTTP API and deployable on public cloud for demonstration. 

Requirements also included guaranteed delivery of emails, for which I have implemented a persistent Outbox with retry
logic and notifications for permanently failed messages.


# Quick Start

## Build and run locally

Requirements: JDK 11+

The project can be built using Gradle build automation. 
The Gradle Wrapper is included in the root directory so pre-installing Gradle is not necessary.

To build the jar file and run tests, use:

```
./gradlew build
```

To run the application server locally, use:

```
./gradlew bootRun
```

By default the API is exposed on port 8000. To verify that the service is up an running, 
try accessing the following URL in a browser:

```
http://localhost:8000/
```

The expected response is:

```
{"status":200,"message":"mail-service is running"}
```

## Testing locally

By default the application is using the *dev* profile which includes test API credentials and a stub version of the 
mail sender API (without SendGrid integration) and runs the sender task with 1 sec fixed period for testing purposes.

See src/main/resources/application-dev.properties:

```
com.aberke.mailservice.provider=stub
```

The stub configuration allows testing the API and database integration without connecting to SendGrid.

For a quick test of the API run the following Http POST (e.g. using curl or other API testing tool like Postman):

```
curl -i --user 'user:password' \
--header "Content-Type: application/json" \
--request POST \
--data '{"from":"abc@abc.com","to":"bca@abc.com","subject":"Hello","body":"World"}' \
localhost:8000/send
```

The expected response is:

```
HTTP/1.1 200
...
{"status":200,"message":"E-mail scheduled for sending"}
```

The API also supports listing validation errors in the response. 
It can be tested for example by sending an invalid from address like “invalid@@.com”:

```
{
	"status": 400,
	"message": "Validation error while processing the request",
	"errors": [{
		"type": "validation",
		"message": "Invalid e-mail address",
		"field": "from",
		"value": "invalid@@.com"
	}]
}
```

API documentation is available at the [Wiki page](docs/ApiDocumentation.md)

## Testing SendGrid integration

Enabling SendGrid integration can be done by adding the following parameters to the *dev* profile:

```
com.aberke.mailservice.provider=sendgrid
com.aberke.mailservice.provider.sendgrid.apikey=${SENDGRID_API_KEY}
```

Note that *SENDGRID_API_KEY* is an environment variable containing the key for SendGrid API access 
(can be obtained after registering on [https://sendgrid.com/](https://sendgrid.com/))

Note that SendGrid requires sender email addresses to be verified before using them. See the website for details. 
Provided that the sender domain is verified we can send emails via the API, e.g.:

```
{
	"from": "RND LNG Sales Team <sales@randomlongitude.com>",
	"to": "berke.andras@gmail.com",
	"subject": "Hello from the Sales Team",
	"body": "Thanks for registering!"
}
```

## Testing the deployed service

**Note Oct 2021: The service is not hosted on AWS anymore, however, the test results are still available on the link below.**

For demonstrational purposes the mail service is deployed on an AWS EC2 instance and connected to a MySQL database storing the emails. 
The service is accessible under the domain [https://randomlongitude.com/mailservice/](https://randomlongitude.com/mailservice/)

The domain has been registered at SendGrid as well, so it can be used as a sender address for functional testing.

Some example emails sent from the service can be found at the [Wiki page](docs/TestingDeployedService.md)

# Technical considerations

A summary of the implementation details and major components of the mail service is available at the [Wiki page](docs/TechnicalConsiderations.md)
