# Technical considerations #

A summary of the implementation details and major components of the mail service.

## API endpoint ##

For receiving mail requests the service is exposing an HTTP endpoint using Spring MVC *RestController*. It is a convenient tool for implementing REST APIs with builtin support for serialization and validation. Parameters are checked using JSR 380 annotations to avoid submitting malformed email addresses or empty bodies. The validation error message responses are customized using *ControllerAdvice*.

## Mail scheduling ##

The core concept of the system is the Outbox, which is a permanent, ordered list of accepted emails scheduled for sending. Storing all pending emails with scheduled sending time allows the addition of various retry strategies using customizable backoff period. Writing the Outbox into a database also makes the system more reliable as it could recover after a restart without losing pending messages (Note that in the *dev* profile the database is in-memory only). For persistence JPA repositories are being used to hide the implementation details of the database driver and allow the domain logic to work with clean abstractions.

## Mail sending ##

The submission of pending outbox mails to the external mail sending service is implemented using a scheduled task defined with the *Scheduled* annotation of the framework. The task is running on a single thread with a fixed time period. Upon execution it is taking the next batch of emails scheduled for sending and submits them to the external API. Batch size and execution period is customizable. Successfully sent mails are put to the log of sent mails (required for bounce handling, see other section) and removed from the Outbox.

In case of failed delivery (e.g. external service is down, or the sender e-mail address is not registered yet) the message is rescheduled, added to the Outbox for a later time to avoid temporary failures impacting delivery. Messages with retry count higher than a preset limit are considered undeliverable and sent to the notification service for alerting the sender.

Depending on the expected load and scalability requirements an alternative solution for processing the requests would be a component using a (3rd party) message queue system and multiple workers set up to handle the load. For the sake of the exercise I choose to implement the single thread sender only. 

## Handling bounces ##

While sender API failures are detected and acted upon immediately, there are other types of errors impacting the requirement of guaranteed mail delivery. Emails can bounce back from the mail server at a later time for various reasons e.g. non-existing addresses or full mailboxes. To handle these types of errors we need to query failed message information from the email service provider and act upon it. In the current implementation of the service this is being done by using another scheduled task for polling the “Bounces” endpoint of the SendGrid API. An alternative solution would be using the real time alerting API if a higher level of responsiveness was required.

The bounce task gets the recently raised bounce events from SendGrid then finds the associated email messages in the log of sent emails. The sender address can be read from the log and the proper alert can be sent via the notification service.

## Alerting ##

While the service incorporates retry logic to avoid temporary failures impacting guaranteed delivery, certain types of failures are permanent and can’t be resolved with retry (e.g. non-existing recipient address). It would make sense to include an alerting system to notify the consumer of the service about undeliverable email. There are various ways to implement such logic depending on the requirements of the client, from simple notification emails to more complex solutions with alerting APIs or message queues. In the current implementation of the service the alerting is done by sending emails back to the sender.

## SendGrid integration ##

The sender API integration is done using a service interface abstraction that hides the implementation details of the SendGrid API. This solution provides a better separation of concerns and also makes the system more flexible as the service provider can be switched to an other service if required. For testing purposes a stub implementation is also available.

## Tests ##

The project contains various automated tests. The functionality of individual classes are validated by unit tests using Mockito and AssertJ. There are also more complex tests available that initialize the Spring application context using the *@WebMvcTest*, *@DataJpaTest* and *@SpringBootTest* annotations to verify if components are properly integrated.