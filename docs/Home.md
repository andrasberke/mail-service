# Home #

mailservice - Java Spring Boot demo service for sending e-mails

## Contents ##

* [Quick Start](QuickStart.md) - How to build, run, test the service locally
* [API documentation](ApiDocumentation.md) - Documentation of the API endpoint
* [Testing the deployed service](TestingDeployedService.md) - Demonstration of the service deployed on AWS
* [Technical considerations](TechnicalConsiderations.md) - Summarizing implementation details