# API Documentation #

## Send ##

Request for submitting email messages.

* URL: /send
* Method: POST
* Content-Type: application/json
* Authentication: Http Basic

### Request Body ###

```
{
  "from": "<EMAIL_ADDRESS>" // string, mandatory
  "to": "<EMAIL_ADDRESS_LIST>", // string, mandatory
  "cc": "<EMAIL_ADDRESS_LIST>", // string, optional
  "bcc": "<EMAIL_ADDRESS_LIST>", // string, optional
  "subject": "<the subject of the email>", // string, mandatory, not empty
  "body": "<the plain text body of the email>" // string, mandatory, not empty
}
```

*EMAIL_ADDRESS* is an address either in standalone email or angle bracket format:

* ```"john.smith@example.com"```
* ```"John Smith <john.smith@example.com>"```

*EMAIL_ADDRESS_LIST* is a comma separated list of *EMAIL_ADDRESS* with at least one entry. E.g.:

* ```"john.smith@example.com"```
* ```"john.smith@example.com, Abraham Smith <abraham.smith@example.com>"```

### Response status ###

* 200 OK - Email has been scheduled for sending
* 400 Bad Request - Malformed request (see body for validation errors)
* 401 Unauthorized - Missing or invalid authentication header
* 500 Internal Server Error - Unexpected error while processing

### Response body ###

```
{
  "status": <STATUS_CODE>, // int, Http status code
  "message": "<human readable status message>", // string
  "errors": [ ERROR, ERROR, ... ] // array, optional
}
```

*ERROR* is in the following format:
```
{
  "type": "validation|generic",
  "message": "<human readable error message>",
  "field": "<name of malformed input field>", // optional, only for validation errors
  "value": "<value of malformed input field>" // optional, only for validation errors
}
```

### Samples ###

Sample request:
```
{
    "from": "RND LNG Sales Team <sales@randomlongitude.com>",
    "to": "berke.andras@gmail.com",
    "subject": "Hello from the Sales Team",
    "body": "Thanks for registering!"
}
```

Sample response:
```
{"status":200,"message":"E-mail scheduled for sending"}
```

Sample request with error:
```
{
    "from": "invalid@@.com",
    "to": "berke.andras@gmail.com",
    "subject": "Hello from the Sales Team",
    "body": "Thanks for registering!"
}
```

Sample error response:
```
{
    "status": 400,
    "message": "Validation error while processing the request",
    "errors": [{
        "type": "validation",
        "message": "Invalid e-mail address",
        "field": "from",
        "value": "invalid@@.com"
    }]
}
```

## Service health ##

Endpoint for checking if the service is up and running.

* URL: /
* Method: GET
* Content-Type: application/json
* Authentication: -

### Response status ###
* 200 OK - Email has been scheduled for sending
* 4xx, 5xx - Unexpected error while routing or processing

### Response body ###
```
{
  "status": <STATUS_CODE>, // int, Http status code
  "message": "<human readable status message>" // string
}
```